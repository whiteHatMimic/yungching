import scrapy;
from bs4 import BeautifulSoup;

print('\033[1;32;40m');

class yungching(scrapy.Spider):
    name = 'yungching'
    start_urls = ['https://buy.yungching.com.tw/region/%E5%8F%B0%E4%B8%AD%E5%B8%82-%E6%B2%99%E9%B9%BF%E5%8D%80_c/']

    def parse(self, response):
        targetNextPage = '';

        for house in response.css('main > .l-main-list > .l-item-list > .m-list-item'):
            yield {
                'title': house.css('.item-info > .item-title.ga_click_trace > h3::text').get(),
                'location': house.css('.item-info > .item-description > span::text').get(),
                'house_type': house.css('.item-info > .item-info-detail > li:nth-child(1)::text').get(),
                'house_age': house.css('.item-info > .item-info-detail > li:nth-child(2)::text').get()
            };

        for page in response.css('main > .l-main-list > .m-pagination > .m-pagination-bd > li'):
            nextpage = page.css('a::attr("ga_label")').get();

            if nextpage == 'buy_page_next':
                nextpage = page.css('a::attr("href")').get();

                if nextpage != '':
                    targetNextPage = 'https://buy.yungching.com.tw/' + nextpage;
                else:
                    targetNextPage = '';

        if targetNextPage != '':
            yield response.follow(targetNextPage, self.parse);